# Consumer

## The challenge

Fazer um processamento através de producer + consumer (backend).

Aqui você fará dois componentes:
- **Producer:** para ler todo o source e enviar para o consumer via TCP client.
- **Consumer:** um TCP server ouvindo esse socket e escrevendo no arquivo output-zap (para imóveis elegíveis para o Zap) e output-viva (para imóveis elegíveis para o Vivareal). O formato de cada linha dos arquivos de output é a mesma do input. A ordem dos dados não importa.

Considere error handling, tracing apropriado, etc. Você decide em qual dos componentes irá implementar a lógica de validação.

Você deverá usar como input o source-3 (~100000 registros):
- http://grupozap-code-challenge.s3-website-us-east-1.amazonaws.com/sources/source-3

As regras de negócio atuais:
- Ele apenas é elegível pro portal ZAP:
  - Quando for aluguel e no mínimo o valor for de R$ 3.500,00.
  - Quando for venda e no mínimo o valor for de R$ 600.000,00.
- Ele apenas é elegível pro portal Vivareal:
  - Quando for aluguel e no máximo o valor for de R$ 4.000,00.
  - Quando for venda e no máximo o valor for de R$ 700.000,00.

Onde:
```
pricingInfos: {
  price: 200000, // Valor de venda
  rentalTotalPrice: 1200, // Valor de aluguel
  businessType: "RENTAL|SALE", // define se é aluguel ou venda
}
```

Agora com a fusão temos algumas alterações que precisam ser feitas. As seguintes regras precisam ser adicionadas às regras já existentes mencionadas no início deste texto:

- Um imóvel não é elegível em NENHUM PORTAL se:
  - Ele tem lat e lon iguais a 0.
- Caso o imóvel seja para venda, ele é elegível para o portal ZAP se:
  - O valor do metro quadrado (chave usableAreas) não pode ser menor/igual a R$ 3.500,00 - apenas considerando imóveis que tenham usableAreas acima de 0.
  - Quando o imóvel estiver dentro do bounding box dos arredores do GrupoZap (descrito abaixo) considere a regra de valor mínimo 10% menor.
- Caso o imóvel seja para aluguel, ele é elegível para o portal Vivareal se:
  - O valor do condomínio não pode ser maior/igual que 30% do valor do aluguel - apenas aplicado para imóveis que tenham um monthlyCondoFee válido e numérico.
  - Quando o imóvel estiver dentro do bounding box dos arredores do GrupoZap (descrito abaixo) considere a regra de valor máximo 50% maior.

Onde:
```
{
  ...
  updatedAt: "2016-11-16T04:14:02Z", // data de atualização do imóvel
  address: {
    geolocation: {
      location: { // latitude/longitude do imóvel
        "lon": -46.716542,
        "lat": -23.502555
      },
    },
  },
  pricingInfos: {
    monthlyCondoFee: "495"
  }
}
```

Bounding Box GrupoZap:
```
minlon: -46.693419
minlat -23.568704
maxlon: -46.641146
maxlat: -23.546686
```

Exemplo de contrato de input/output.

Os JSON com dados estão minificados, então para facilitar seu desenvolvimento, aqui um exemplo dele pretty:
```
[
  {
    "usableAreas": 69,
    "listingType": "USED",
    "createdAt": "2016-11-16T04:14:02Z",
    "listingStatus": "ACTIVE",
    "id": "some-id",
    "parkingSpaces": 1,
    "updatedAt": "2016-11-16T04:14:02Z",
    "owner": false,
    "images": [
      "https://resizedimgs.vivareal.com/crop/400x300/vr.images.sp/some-id1.jpg",
      "https://resizedimgs.vivareal.com/crop/400x300/vr.images.sp/some-id2.jpg",
      "https://resizedimgs.vivareal.com/crop/400x300/vr.images.sp/some-id3.jpg",
      "https://resizedimgs.vivareal.com/crop/400x300/vr.images.sp/some-id4.jpg",
      "https://resizedimgs.vivareal.com/crop/400x300/vr.images.sp/some-id5.jpg"
    ],
    "address": {
      "city": "",
      "neighborhood": "",
      "geoLocation": {
        "precision": "ROOFTOP",
        "location": {
          "lon": -46.716542,
          "lat": -23.502555
        }
      }
    },
    "bathrooms": 2,
    "bedrooms": 3,
    "pricingInfos": {
      "yearlyIptu": "0",
      "price": "405000",
      "businessType": "SALE",
      "monthlyCondoFee": "495"
    }
  },
  {
    "usableAreas": 70,
    "listingType": "USED",
    "createdAt": "2017-04-22T18:39:31.138Z",
    "listingStatus": "ACTIVE",
    "id": "some-id-2",
    "parkingSpaces": 1,
    "updatedAt": "2017-04-22T18:39:31.138Z",
    "owner": false,
    "images": [
      "https://resizedimgs.vivareal.com/crop/400x300/vr.images.sp/some-id-2-1.jpg",
      "https://resizedimgs.vivareal.com/crop/400x300/vr.images.sp/some-id-2-2.jpg",
      "https://resizedimgs.vivareal.com/crop/400x300/vr.images.sp/some-id-2-3.jpg",
      "https://resizedimgs.vivareal.com/crop/400x300/vr.images.sp/some-id-2-4.jpg",
      "https://resizedimgs.vivareal.com/crop/400x300/vr.images.sp/some-id-2-5.jpg"
    ],
    "address": {
      "city": "",
      "neighborhood": "",
      "geoLocation": {
        "precision": "ROOFTOP",
        "location": {
          "lon": -46.716542,
          "lat": -23.502555
        }
      }
    },
    "bathrooms": 1,
    "bedrooms": 2,
    "pricingInfos": {
      "yearlyIptu": "60",
      "price": "276000",
      "businessType": "SALE",
      "monthlyCondoFee": "0"
    }
  }
]
```

## The result

### Producer
The Producer was implemented in another project.
- https://gitlab.com/alexandreishida/producer

### Requirements
- Python 3.4 or newer

### Installation
```bash
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

### Test
```bash
$ PYTHONPATH=src/ pytest -v tests/
```

### Run
```bash
$ PYTHONPATH=src/ python src/main.py
```

### Sample of output
```bash
$ PYTHONPATH=src/ python src/main.py
Delete previous output files
Start Server on port 4500
Start Consumer
Nothing to consume
Nothing to consume
Nothing to consume
Receiving from 127.0.0.1: ................................................................................................................................................................................................................................................................................................................................................................................................................!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Nothing to consume
Nothing to consume
Nothing to consume
```

### CI
- https://gitlab.com/alexandreishida/consumer/pipelines

### Docker run
```bash
$ docker login registry.gitlab.com
$ docker run --rm -it -p 4500:4500 -v /opt/consumer/data:/app/data registry.gitlab.com/alexandreishida/consumer:0.1
```

### Monitoring
1. In terminal, start docker compose.
```bash
$ docker-compose build
$ docker-compose up
```
2. Access http://localhost:5601/app/kibana#/management/kibana/objects?_g=()
3. Import the configuration file docker/kibana/config/saved-objects.json
4. Go to Dashboard menu > dashboard

![alt text](https://gitlab.com/alexandreishida/consumer/raw/master/kibana-dashboard.png "Kibana")

### To Do
- Optimize append strategy
- Asynchronous handlers in server