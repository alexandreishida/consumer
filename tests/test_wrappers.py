import pytest

from wrappers import JsonWrapper

@pytest.fixture
def jsonw():
    json = """
      {
        "usableAreas": 70,
        "address": {
          "geoLocation": {
            "location": {
              "lat": -1,
              "lon": 1
            }
          }
        },
        "pricingInfos": {
          "businessType": "SALE",
          "price": "675000",
          "monthlyCondoFee": "1025"
        }
      }
    """

    yield JsonWrapper(json)

class TestJsonWrapper:

    def test_coordinates(self, jsonw):
        assert jsonw.coordinates() == {"lat": -1, "lon": 1}

    def test_latitude(self, jsonw):
        assert jsonw.latitude() == -1

    def test_longitude(self, jsonw):
        assert jsonw.longitude() == 1

    def test_usable_areas(self, jsonw):
        assert jsonw.usable_areas() == 70

    def test_business_type(self, jsonw):
        assert jsonw.business_type() == "SALE"

    def test_for_sale(self, jsonw):
        assert jsonw.for_sale()

    def test_for_rental(self, jsonw):
        assert jsonw.for_rental() == False

    def test_price(self, jsonw):
        assert jsonw.price() == 675000

    def test_monthly_condo_fee(self, jsonw):
        assert jsonw.monthly_condo_fee() == 1025
