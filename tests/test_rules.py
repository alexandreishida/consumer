import pytest
from unittest.mock import Mock

from rules import CommonRules, ZapRules, VivarealRules
from wrappers import JsonWrapper

class TestCommonRules:

    @pytest.fixture
    def rules(self):
        yield CommonRules

    def test_invalid_coordinates(self, rules):
        jsonw = JsonWrapper("{}")
        jsonw.latitude = Mock(return_value = 0)
        jsonw.longitude = Mock(return_value = 0)

        assert rules.invalid_coordinates(jsonw)

    def test_in_bounding_box(self, rules):
        jsonw = JsonWrapper("{}")

        jsonw.latitude = Mock(return_value = -23.55)
        jsonw.longitude = Mock(return_value = -46.65)
        assert rules.in_bounding_box(jsonw)

        jsonw.latitude = Mock(return_value = 23)
        jsonw.longitude = Mock(return_value = -46.65)
        assert not rules.in_bounding_box(jsonw)

class TestZapRules:

    @pytest.fixture
    def rules(self):
        CommonRules.in_bounding_box = Mock(return_value=False)

        yield ZapRules

    def test_magic_price(self, rules):
        jsonw = JsonWrapper("{}")
        jsonw.business_type = Mock(return_value = "SALE")
        jsonw.price = Mock(return_value = 100000)

        assert rules.magic_price(jsonw) == 100000

        CommonRules.in_bounding_box = Mock(return_value=True)
        assert rules.magic_price(jsonw) == 90000

    def test_invalid_price_for_rental(self, rules):
        jsonw = JsonWrapper("{}")
        jsonw.business_type = Mock(return_value = "RENTAL")

        jsonw.price = Mock(return_value = 3499)
        assert rules.invalid_price_for_rental(jsonw)
        
        jsonw.price = Mock(return_value = 3500)
        assert not rules.invalid_price_for_rental(jsonw)

    def test_invalid_price_for_sale(self, rules):
        jsonw = JsonWrapper("{}")
        jsonw.business_type = Mock(return_value = "SALE")

        jsonw.price = Mock(return_value = 599999)
        assert rules.invalid_price_for_sale(jsonw)
        
        jsonw.price = Mock(return_value = 600000)
        assert not rules.invalid_price_for_sale(jsonw)

    def test_invalid_price_per_meter_for_sale(self, rules):
        jsonw = JsonWrapper("{}")
        jsonw.business_type = Mock(return_value = "SALE")
        jsonw.price = Mock(return_value = 350000)

        jsonw.usable_areas = Mock(return_value = 100)
        assert rules.invalid_price_per_meter_for_sale(jsonw)
        
        jsonw.usable_areas = Mock(return_value = 99)
        assert not rules.invalid_price_per_meter_for_sale(jsonw)

class TestVivarealRules:

    @pytest.fixture
    def rules(self):
        CommonRules.in_bounding_box = Mock(return_value=False)

        yield VivarealRules

    def test_magic_price(self, rules):
        jsonw = JsonWrapper("{}")
        jsonw.business_type = Mock(return_value = "RENTAL")
        jsonw.price = Mock(return_value = 100000)

        assert rules.magic_price(jsonw) == 100000

        CommonRules.in_bounding_box = Mock(return_value=True)
        assert rules.magic_price(jsonw) == 150000

    def test_invalid_price_for_rental(self, rules):
        jsonw = JsonWrapper("{}")
        jsonw.business_type = Mock(return_value = "RENTAL")

        jsonw.price = Mock(return_value = 4001)
        assert rules.invalid_price_for_rental(jsonw)
        
        jsonw.price = Mock(return_value = 4000)
        assert not rules.invalid_price_for_rental(jsonw)

    def test_invalid_monthly_condo_fee_for_rental(self, rules):
        jsonw = JsonWrapper("{}")
        jsonw.business_type = Mock(return_value = "RENTAL")
        jsonw.price = Mock(return_value = 1000)

        jsonw.monthly_condo_fee = Mock(return_value = 300)
        assert rules.invalid_monthly_condo_fee_for_rental(jsonw)
        
        jsonw.monthly_condo_fee = Mock(return_value = 299)
        assert not rules.invalid_monthly_condo_fee_for_rental(jsonw)

    def test_invalid_price_for_sale(self, rules):
        jsonw = JsonWrapper("{}")
        jsonw.business_type = Mock(return_value = "SALE")

        jsonw.price = Mock(return_value = 700001)
        assert rules.invalid_price_for_sale(jsonw)
        
        jsonw.price = Mock(return_value = 700000)
        assert not rules.invalid_price_for_sale(jsonw)
