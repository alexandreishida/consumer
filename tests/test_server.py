import pytest
import socket
from threading import Thread
import time

from server import Server, queue

SERVER_HOST = "localhost"
SERVER_PORT = 4449

@pytest.fixture(scope="module")
def server():
    server = Server(SERVER_HOST, SERVER_PORT)

    thread = Thread(target=server.start)
    thread.daemon = True
    thread.start()

    yield server

    server.stop()

@pytest.fixture
def sock(server):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((SERVER_HOST, SERVER_PORT))
    
    yield sock

    sock.close()

def test_receive(sock):
    sock.sendall(bytes("foo\n", "utf-8"))
    sock.sendall(bytes("bar\n", "utf-8"))
    sock.sendall(bytes("baz\n", "utf-8"))

    time.sleep(0.5)

    assert len(queue) == 3
    assert list(queue) == ["foo\n", "bar\n", "baz\n"]
