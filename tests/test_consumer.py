from collections import deque
from threading import Thread
import time
from unittest.mock import Mock

from consumer import Consumer

def test_consume():
    queue = deque()
    item_handler = Mock()

    consumer = Consumer(queue, item_handler, 0.1)

    thread = Thread(target=consumer.start)
    thread.daemon = True
    thread.start()

    queue.append("foo")
    queue.append("bar")
    queue.append("baz")

    time.sleep(0.2)

    assert len(queue) == 0
