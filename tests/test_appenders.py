import os
import pytest
from unittest.mock import Mock
import ujson

from appenders import Appender, ZapAppender, VivarealAppender
from rules import CommonRules, ZapRules, VivarealRules
from wrappers import JsonWrapper

class TestAppender:

    def test_validate(self):
        def blank_value(value):
            return value == ""

        class MyAppender(Appender):
            filter_rules = [
                blank_value
            ]

        appender = MyAppender("")

        assert appender.validate("foo")
        assert not appender.validate("")

    def test_append(self):
        file_path = "data/test-output"

        if os.path.exists(file_path):
            os.remove(file_path)

        appender = Appender(file_path)

        appender.validate = Mock(return_value=True)

        appender.append(JsonWrapper('{"foo":"bar"}\n'))
        appender.append(JsonWrapper('{"baz":null}\n'))

        assert open(file_path, "r").read() == '{"foo":"bar"}\n{"baz":null}\n'

class TestZapAppender:

    def test_inheritance(self):
        assert issubclass(ZapAppender, Appender)

    def test_filter_rules(self):
        assert ZapAppender.filter_rules == [
            CommonRules.invalid_coordinates,
            ZapRules.invalid_price_for_rental,
            ZapRules.invalid_price_for_sale,
            ZapRules.invalid_price_per_meter_for_sale
        ]

class TestVivarealAppender:

    def test_inheritance(self):
        assert issubclass(VivarealAppender, Appender)

    def test_filter_rules(self):
        assert VivarealAppender.filter_rules == [
            CommonRules.invalid_coordinates,
            VivarealRules.invalid_price_for_rental,
            VivarealRules.invalid_monthly_condo_fee_for_rental,
            VivarealRules.invalid_price_for_sale
        ]
