FROM python:3

WORKDIR /app

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

VOLUME /app/data
VOLUME /app/logs

EXPOSE 4500

ENV PYTHONPATH=src/

CMD ["python", "src/main.py"]