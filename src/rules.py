class CommonRules:

    minlat = -23.568704
    maxlat = -23.546686
    minlon = -46.693419
    maxlon = -46.641146

    @classmethod
    def invalid_coordinates(cls, jsonw):
        if jsonw.latitude() == 0 and jsonw.longitude() == 0:
            return True
    
    @classmethod
    def in_bounding_box(cls, jsonw):
        lat = jsonw.latitude()
        lon = jsonw.longitude()

        if lat < cls.minlat or lat > cls.maxlat:
            return False

        if lon < cls.minlon or lon > cls.maxlon:
            return False

        return True

class ZapRules:

    @classmethod
    def magic_price(cls, jsonw):
        price = jsonw.price()
        if jsonw.for_sale():
            if CommonRules.in_bounding_box(jsonw):
                price = price * 0.9
        return price

    @classmethod
    def invalid_price_for_rental(cls, jsonw):
        if jsonw.for_rental():
            if cls.magic_price(jsonw) < 3500:
                return True
        return False
    
    @classmethod
    def invalid_price_for_sale(cls, jsonw):
        if jsonw.for_sale():
            if cls.magic_price(jsonw) < 600000:
                return True
        return False

    @classmethod
    def invalid_price_per_meter_for_sale(cls, jsonw):
        if jsonw.for_sale():
            if jsonw.usable_areas() > 0:
                if cls.magic_price(jsonw) / jsonw.usable_areas() <= 3500:
                    return True
        return False

class VivarealRules:

    @classmethod
    def magic_price(cls, jsonw):
        price = jsonw.price()
        if jsonw.for_rental():
            if CommonRules.in_bounding_box(jsonw):
                price = price * 1.5
        return price

    @classmethod
    def invalid_price_for_rental(cls, jsonw):
        if jsonw.for_rental():
            if cls.magic_price(jsonw) > 4000:
                return True
        return False

    @classmethod
    def invalid_monthly_condo_fee_for_rental(cls, jsonw):
        if jsonw.for_rental():
            if jsonw.monthly_condo_fee():
                if jsonw.monthly_condo_fee() >= cls.magic_price(jsonw) * 0.3:
                    return True
        return False
    
    @classmethod
    def invalid_price_for_sale(cls, jsonw):
        if jsonw.for_sale():
            if cls.magic_price(jsonw) > 700000:
                return True
        return False
