import ujson

class JsonWrapper:

    def __init__(self, json):
        self.json = json
        self.data = ujson.loads(json)

    def coordinates(self):
        return self.data["address"]["geoLocation"]["location"]

    def latitude(self):
        return self.coordinates()["lat"]

    def longitude(self):
        return self.coordinates()["lon"]

    def usable_areas(self):
        return self.data["usableAreas"]

    def business_type(self):
        return self.data["pricingInfos"]["businessType"]
    
    def for_sale(self):
        return self.business_type() == "SALE"
    
    def for_rental(self):
        return self.business_type() == "RENTAL"

    def price(self):
        value = self.data["pricingInfos"]["price"]
        return int(value)

    def monthly_condo_fee(self):
        value = self.data["pricingInfos"].get("monthlyCondoFee")
        return int(value) if value else None
