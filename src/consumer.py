import time

class Consumer:

    def __init__(self, queue, item_handler, sleep_interval = 1):
        self.queue = queue
        self.item_handler = item_handler
        self.sleep_interval = sleep_interval
        self.started = False

    def start(self):
        self.started = True
        self.consume()

    def stop(self):
        self.started = False

    def stopped(self):
        return not self.started

    def consume(self):
        idle = True
        while self.started:
            try:
                item = self.queue.pop()
                self.item_handler(item)
                idle = False
            except IndexError:
                if not idle:
                    idle = True
                    print("")

                print("Nothing to consume")
                time.sleep(self.sleep_interval)
