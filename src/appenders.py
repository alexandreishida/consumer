from log import log
from rules import CommonRules, ZapRules, VivarealRules

class Appender:

    filter_rules = []

    def __init__(self, file_path):
        self.file_path = file_path

    def validate(self, jsonw):
        for f in self.filter_rules:
            if f(jsonw): return False
        return True

    def append(self, jsonw):
        if self.validate(jsonw):
            f = open(self.file_path, "a")
            f.write(jsonw.json)
            f.close

            log.log("appended", appender=self.__class__.__name__, data=jsonw.data)
        else:
            log.log("excluded", appender=self.__class__.__name__, data=jsonw.data)

class ZapAppender(Appender):

    filter_rules = [
        CommonRules.invalid_coordinates,
        ZapRules.invalid_price_for_rental,
        ZapRules.invalid_price_for_sale,
        ZapRules.invalid_price_per_meter_for_sale
    ]

class VivarealAppender(Appender):

    filter_rules = [
        CommonRules.invalid_coordinates,
        VivarealRules.invalid_price_for_rental,
        VivarealRules.invalid_monthly_condo_fee_for_rental,
        VivarealRules.invalid_price_for_sale
    ]
