import structlog

structlog.configure(
    processors=[
        structlog.processors.TimeStamper(fmt="iso"),
        structlog.processors.UnicodeDecoder(),
        structlog.processors.JSONRenderer()
    ],
    context_class=dict
)

log_file = open("logs/app.log", "a")

log = structlog.wrap_logger(structlog.PrintLogger(log_file))
