import os
from os import environ
from threading import Thread

from appenders import ZapAppender, VivarealAppender
from consumer import Consumer
from server import Server, queue
from wrappers import JsonWrapper


SERVER_PORT = int(environ.get("SERVER_PORT", 4500))
OUTPUT_ZAP = "data/output-zap"
OUTPUT_VIVA = "data/output-viva"


print("Delete previous output files")

if os.path.exists(OUTPUT_ZAP):
    os.remove(OUTPUT_ZAP)

if os.path.exists(OUTPUT_VIVA):
    os.remove(OUTPUT_VIVA)


print("Start Server on port {}".format(SERVER_PORT))

queue_server = Server("0.0.0.0", SERVER_PORT)

thread = Thread(target=queue_server.start)
thread.daemon = True
thread.start()


print("Start Consumer")

zap_appender = ZapAppender(OUTPUT_ZAP)
viva_appender = VivarealAppender(OUTPUT_VIVA)

def item_handler(json):
    print("!", end="", flush=True)
    try:
        jsonw = JsonWrapper(json)
        zap_appender.append(jsonw)
        viva_appender.append(jsonw)
    except Exception as ex:
        print(json)
        raise ex

Consumer(queue, item_handler).start()
