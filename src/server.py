from collections import deque
import socket
import socketserver

from log import log

queue = deque()

class Server:

    def __init__(self, host, port):
        self.tcpserver = socketserver.TCPServer((host, port), SocketHandler)

    def start(self):
        self.tcpserver.serve_forever()

    def stop(self):
        self.tcpserver.shutdown()
        self.tcpserver.server_close()

class SocketHandler(socketserver.StreamRequestHandler):

    def handle(self):
        client_address = self.client_address[0]
        print("Receiving from {}: ".format(client_address), end="", flush=True)

        while True:
            line = self.rfile.readline()

            if not line:
                break

            if len(line) > 0:
                print(".", end="", flush=True)

                line = line.decode('utf-8')
                queue.append(line)

                log.log("receipt", client_address=client_address, raw=line)


if __name__ == "__main__":
    print("Start Server")
    Server("localhost", 4500).start()
